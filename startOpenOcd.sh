##################################################################
# OpenOcd Script: Same backend used within debug extension
#  within vbcode with the exception of a defined log file.
#  ToDo: /home/cswift needs replacing $HOME or some universal
#    directory
##################################################################

# OpenOcd
"/home/$(whoami)/.toolchain/stm32/v0.0.0/bin/openocd" "-c" "gdb_port 50000" "-s" "/home/$(whoami)/.toolchain/stm32/v0.0.0/share/openocd/scripts" \
	"-f" "interface/stlink.cfg" \
	"-f" "target/stm32f4x.cfg"  \
	"-c" "tpiu config internal rawlog.dat uart off 96000000 2000000" 
